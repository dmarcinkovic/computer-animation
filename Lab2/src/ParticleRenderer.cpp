//
// Created by david on 23. 11. 2021..
//

#include <glm/ext/matrix_transform.hpp>

#include "ParticleRenderer.h"
#include "Maths.h"
#include "Constants.h"
#include "Display.h"

ParticleRenderer::ParticleRenderer(Loader &loader)
{
	std::vector<float> vertices{-0.5f, 0.5f, -0.5f, -0.5f, 0.5f, 0.5f, 0.5f, -0.5f};
	model = loader.loadToVao(vertices, 2);

	vbo = loader.createEmptyVBO(INSTANCE_DATA_LEN * MAX_INSTANCES);
	Loader::addInstancedAttribute(model.vaoId, vbo, 1, 4, INSTANCE_DATA_LEN, 0);
	Loader::addInstancedAttribute(model.vaoId, vbo, 2, 4, INSTANCE_DATA_LEN, 4);
	Loader::addInstancedAttribute(model.vaoId, vbo, 3, 4, INSTANCE_DATA_LEN, 8);
	Loader::addInstancedAttribute(model.vaoId, vbo, 4, 4, INSTANCE_DATA_LEN, 12);
}

void ParticleRenderer::render(const std::vector<Particle> &particles, const Camera &camera)
{
	glm::mat4 viewMatrix = Maths::createViewMatrix(camera);
	prepare();

	std::vector<float> data;
	for (Particle particle : particles)
	{
		updateModelViewMatrix(particle.position, particle.rotation, particle.scale, viewMatrix, data);
	}

	if (!particles.empty())
	{
		glActiveTexture(GL_TEXTURE0);
		GLuint texture = particles.front().texture;
		glBindTexture(GL_TEXTURE_2D, texture);

		Loader::updateVBO(vbo, data, particles.size() * INSTANCE_DATA_LEN);
		glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, model.vertexCount, particles.size());

		glBindTexture(GL_TEXTURE_2D, 0);

	}

	finish();
}

void ParticleRenderer::prepare()
{
	shader.start();
	glBindVertexArray(model.vaoId);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
	glEnable(GL_BLEND);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glDepthMask(GL_FALSE);

	loadProjectionMatrix();
}

void ParticleRenderer::finish()
{
	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
	glDisableVertexAttribArray(4);
	glBindVertexArray(0);
	Shader::stop();
}

void ParticleRenderer::updateModelViewMatrix(const glm::vec3 &position, float rotation, float scale,
											 const glm::mat4 &viewMatrix, std::vector<float> &data)
{
	glm::mat4 modelMatrix{1.0f};
	modelMatrix = glm::translate(modelMatrix, position);

	modelMatrix[0][0] = viewMatrix[0][0];
	modelMatrix[0][1] = viewMatrix[1][0];
	modelMatrix[0][2] = viewMatrix[2][0];
	modelMatrix[1][0] = viewMatrix[0][1];
	modelMatrix[1][1] = viewMatrix[1][1];
	modelMatrix[1][2] = viewMatrix[2][1];
	modelMatrix[2][0] = viewMatrix[0][2];
	modelMatrix[2][1] = viewMatrix[1][2];
	modelMatrix[2][2] = viewMatrix[2][2];

	modelMatrix = glm::rotate(modelMatrix, glm::radians(rotation), glm::vec3{0, 0, 1});
	modelMatrix = glm::scale(modelMatrix, glm::vec3{scale});

	glm::mat4 modelViewMatrix = viewMatrix * modelMatrix;

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			data.emplace_back(modelViewMatrix[i][j]);
		}
	}
}

void ParticleRenderer::loadProjectionMatrix()
{
	float fov = Constants::FOV;
	float near = Constants::NEAR;
	float far = Constants::FAR;

	auto width = static_cast<float>(Display::getWidth());
	auto height = static_cast<float>(Display::getHeight());
	glm::mat4 projectionMatrix = Maths::createProjectionMatrix(fov, far, near, width, height);
	shader.loadProjectionMatrix(projectionMatrix);
}
