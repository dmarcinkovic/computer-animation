//
// Created by david on 14. 10. 2021..
//

#ifndef LAB1_CONSTANTS_H
#define LAB1_CONSTANTS_H

struct Constants
{
	constexpr static float FOV = 60.0f;
	constexpr static float NEAR = 0.1f;
	constexpr static float FAR = 100.0f;
};

#endif //LAB1_CONSTANTS_H
