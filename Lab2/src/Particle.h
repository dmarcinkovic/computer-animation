//
// Created by david on 23. 11. 2021..
//

#ifndef LAB2_PARTICLE_H
#define LAB2_PARTICLE_H

#include <glm/glm.hpp>
#include <GL/glew.h>

struct Particle
{
	glm::vec3 position{};
	glm::vec3 velocity{};

	float gravityEffect{};
	float lifeLength{};
	float rotation{};
	float scale{};

	float elapsedTime{};

	GLuint texture{};

	bool update();

	Particle() = default;

	Particle(GLuint texture, glm::vec3 position, glm::vec3 velocity, float gravityEffect, float lifeLength,
			 float rotation, float scale);
};


#endif //LAB2_PARTICLE_H
