#version 450 core

layout (location = 0) in vec2 position;
layout (location = 1) in mat4 modelViewMatrix;

uniform mat4 projectionMatrix;

out vec2 textureCoordinates;

void main()
{
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 0.0f, 1.0f);

    textureCoordinates = position + vec2(0.5, 0.5);
    textureCoordinates.y = 1.0 - textureCoordinates.y;
}
