//
// Created by david on 23. 11. 2021..
//

#ifndef LAB2_PARTICLERENDERER_H
#define LAB2_PARTICLERENDERER_H

#include "Loader.h"
#include "ParticleShader.h"
#include "Particle.h"
#include "Camera.h"

class ParticleRenderer
{
private:
	static constexpr int MAX_INSTANCES = 10000;
	static constexpr int INSTANCE_DATA_LEN = 16;

	Model model;
	ParticleShader shader;

	GLuint vbo;

public:
	explicit ParticleRenderer(Loader &loader);

	void render(const std::vector<Particle> &particles, const Camera &camera);

private:
	void prepare();

	static void updateModelViewMatrix(const glm::vec3 &position, float rotation, float scale, const glm::mat4 &viewMatrix,
							   std::vector<float> &data);

	static void finish();

	void loadProjectionMatrix();
};


#endif //LAB2_PARTICLERENDERER_H
