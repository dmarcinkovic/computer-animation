//
// Created by david on 21. 03. 2020..
//

#ifndef LAB3_LOADER_H
#define LAB3_LOADER_H

#include <vector>
#include <GL/glew.h>

struct Model
{
	GLuint vaoId{};
	unsigned int vertexCount{};

	Model(GLuint vaoId, unsigned vertexCount);

	Model() = default;
};

class Loader
{
private:
	std::vector<GLuint> vbos;
	std::vector<GLuint> vaos;
	std::vector<GLuint> attributes;
	std::vector<GLuint> textures;

public:
	~Loader();

	Model loadToVao(const std::vector<float> &position, const std::vector<unsigned> &indices);

	Model loadToVao(const std::vector<float> &points, GLint dimension);

	void unbindVao() const;

	void bindVao(const Model &model) const;

	GLuint createEmptyVBO(GLsizeiptr vertexCount);

	static void updateVBO(GLuint vbo, const std::vector<float> &data, GLsizeiptr sizeOfData);

	static void addInstancedAttribute(GLuint vao, GLuint vbo, GLuint attribute, int vertexCount,
									  GLsizei instancedDataLength, int offset);

	GLuint loadTexture(const char *texture);

private:
	GLuint createVao();

	void bindIndexBuffer(const std::vector<unsigned> &indices);

	void storeDataInAttributeList(GLuint attributeNumber, const std::vector<float> &data, GLint coordinateSize);

	static void loadImage(const char *texture);
};


#endif //LAB3_LOADER_H
