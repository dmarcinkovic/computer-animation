#version 450 core

out vec4 outColor;

in vec2 textureCoordinates;
uniform sampler2D particle;

void main()
{
    outColor = texture(particle, textureCoordinates);
}
