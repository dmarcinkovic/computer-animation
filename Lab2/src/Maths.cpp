//
// Created by david on 21. 03. 2020..
//

#include <glm/gtc/matrix_transform.hpp>

#include "Maths.h"

glm::mat4 Maths::createViewMatrix(const Camera &camera)
{
	glm::mat4 viewMatrix{1.0};
	viewMatrix = glm::rotate(viewMatrix, glm::radians(camera.getPitch()), glm::vec3{1, 0, 0});
	viewMatrix = glm::rotate(viewMatrix, glm::radians(camera.getYaw()), glm::vec3{0, 1, 0});

	viewMatrix = glm::translate(viewMatrix, -camera.getPosition());
	return viewMatrix;
}

glm::mat4
Maths::createTransformationMatrix(const glm::vec3 &translation, const glm::vec3 &rotation, const glm::vec3 &scale)
{
	glm::mat4 matrix{1.0};

	matrix = glm::translate(matrix, translation);

	matrix = glm::rotate(matrix, rotation.x, glm::vec3{1, 0, 0});
	matrix = glm::rotate(matrix, rotation.y, glm::vec3{0, 1, 0});
	matrix = glm::rotate(matrix, rotation.z, glm::vec3{0, 0, 1});

	matrix = glm::scale(matrix, scale);

	return matrix;
}

glm::mat4 Maths::createProjectionMatrix(float fieldOfView, float farPlane, float nearPlane, float width, float height)
{
	float aspectRatio = width / height;

	return glm::perspective(glm::radians(fieldOfView), aspectRatio, nearPlane, farPlane);
}
