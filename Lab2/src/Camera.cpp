//
// Created by david on 21. 03. 2020..
//

#include "Camera.h"

const glm::vec3 &Camera::getPosition() const
{
	return position;
}

float Camera::getPitch() const
{
	return pitch;
}

float Camera::getYaw() const
{
	return yaw;
}

void Camera::mouseScroll(double offset)
{
	if (offset > 0)
	{
		position.z -= 0.1f;
	} else
	{
		position.z += 0.1f;
	}
}
