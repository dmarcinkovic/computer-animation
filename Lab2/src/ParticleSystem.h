//
// Created by david on 27. 11. 2021..
//

#ifndef LAB2_PARTICLESYSTEM_H
#define LAB2_PARTICLESYSTEM_H

#include <vector>
#include <glm/glm.hpp>

#include "Particle.h"

class ParticleSystem
{
private:
	float particlesPerSecond;
	float speed;
	float gravity;
	float lifeLength;

	GLuint texture;

public:
	ParticleSystem(GLuint texture, float particlesPerSecond, float speed, float gravity, float lifeLength);

	void generateParticles(const glm::vec3 &center, std::vector<Particle> &particles);

	static void update(std::vector<Particle> &particles);

private:
	[[nodiscard]] Particle emitParticle(const glm::vec3 &center) const;
};


#endif //LAB2_PARTICLESYSTEM_H
