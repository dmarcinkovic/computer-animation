//
// Created by david on 27. 11. 2021..
//

#include "ParticleSystem.h"
#include "Display.h"
#include "Util.h"

ParticleSystem::ParticleSystem(GLuint texture, float particlesPerSecond, float speed, float gravity, float lifeLength)
		: texture(texture), particlesPerSecond(particlesPerSecond), speed(speed), gravity(gravity),
		  lifeLength(lifeLength)
{

}

void ParticleSystem::generateParticles(const glm::vec3 &center, std::vector<Particle> &particles)
{
	auto delta = static_cast<float>(Display::getFrameTime());
	int particlesToCreate = std::floor(particlesPerSecond * delta);

	for (int i = 0; i < particlesToCreate; ++i)
	{
		particles.emplace_back(emitParticle(center));
	}
}

Particle ParticleSystem::emitParticle(const glm::vec3 &center) const
{
	float dirX = Util::getRandom() * 2.0f - 1.0f;
	float dirZ = Util::getRandom() * 2.0f - 1.0f;

	glm::vec3 velocity{dirX, 1, dirZ};
	velocity = glm::normalize(velocity) * speed;

	return Particle{texture, center, velocity, gravity, lifeLength, 0, 1};
}

void ParticleSystem::update(std::vector<Particle> &particles)
{
	particles.erase(std::remove_if(particles.begin(), particles.end(), [&](Particle &particle) {
		return !particle.update();
	}), particles.end());
}
