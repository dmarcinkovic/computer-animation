//
// Created by david on 21. 03. 2020..
//

#ifndef LAB3_MATHS_H
#define LAB3_MATHS_H

#include <glm/glm.hpp>

#include "Camera.h"

class Maths
{
public:
	static glm::mat4 createViewMatrix(const Camera &camera);

	static glm::mat4 createTransformationMatrix(const glm::vec3 &translation,
												const glm::vec3 &rotation = glm::vec3{0.0f},
												const glm::vec3 &scale = glm::vec3{1.0f});

	static glm::mat4
	createProjectionMatrix(float fieldOfView, float farPlane, float nearPlane, float width, float height);
};


#endif //LAB3_MATHS_H
