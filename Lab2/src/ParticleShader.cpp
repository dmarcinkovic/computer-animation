//
// Created by david on 23. 11. 2021..
//

#include "ParticleShader.h"

ParticleShader::ParticleShader()
	: Shader(VERTEX_FILE, FRAGMENT_FILE)
{
	getUniformLocations();
}

void ParticleShader::getUniformLocations()
{
//	locationModelViewMatrix = glGetUniformLocation(program, "modelViewMatrix");
	locationProjectionMatrix = glGetUniformLocation(program, "projectionMatrix");
}

void ParticleShader::loadViewModelMatrix(const glm::mat4 &modelViewMatrix) const
{
	loadMatrix(locationModelViewMatrix,  modelViewMatrix);
}

void ParticleShader::loadProjectionMatrix(const glm::mat4 &projectionMatrix) const
{
	loadMatrix(locationProjectionMatrix, projectionMatrix);
}

void ParticleShader::loadMatrix(GLint location, const glm::mat4 &matrix)
{
	glUniformMatrix4fv(location, 1, GL_FALSE, &matrix[0][0]);
}