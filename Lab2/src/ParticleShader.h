//
// Created by david on 23. 11. 2021..
//

#ifndef LAB2_PARTICLESHADER_H
#define LAB2_PARTICLESHADER_H

#include "Shader.h"

class ParticleShader : public Shader
{
private:
	static constexpr const char *VERTEX_FILE = "src/ParticleVertexShader.glsl";
	static constexpr const char *FRAGMENT_FILE = "src/ParticleFragmentShader.glsl";

	GLint locationModelViewMatrix{};
	GLint locationProjectionMatrix{};

public:
	ParticleShader();

	void loadViewModelMatrix(const glm::mat4 &modelViewMatrix) const;

	void loadProjectionMatrix(const glm::mat4 &projectionMatrix) const;

private:
	void getUniformLocations();

	static void loadMatrix(GLint location, const glm::mat4 &matrix);
};


#endif //LAB2_PARTICLESHADER_H
