//
// Created by david on 21. 03. 2020..
//

#ifndef LAB3_CAMERA_H
#define LAB3_CAMERA_H

#include <glm/glm.hpp>

struct Camera
{
	glm::vec3 position{};

	float pitch{};
	float yaw{};

	glm::vec3 rotation{};

	[[nodiscard]] const glm::vec3 &getPosition() const;

	[[nodiscard]] float getPitch() const;

	[[nodiscard]] float getYaw() const;

	void mouseScroll(double offset);
};


#endif //LAB3_CAMERA_H
