
#include <stdexcept>
#include "Loader.h"
#include "stb_image.h"

Loader::~Loader()
{
	for (auto &vao: vaos)
	{
		glDeleteVertexArrays(1, &vao);
	}

	for (auto &vbo: vbos)
	{
		glDeleteBuffers(1, &vbo);
	}

	glDeleteTextures(textures.size(), textures.data());
}

Model Loader::loadToVao(const std::vector<float> &position, const std::vector<unsigned> &indices)
{
	GLuint vao = createVao();

	bindIndexBuffer(indices);
	storeDataInAttributeList(0, position, 3);

	return {vao, static_cast<unsigned int>(indices.size())};
}

Model Loader::loadToVao(const std::vector<float> &points, GLint dimension)
{
	GLuint vao = createVao();

	storeDataInAttributeList(0, points, dimension);
	return {vao, static_cast<unsigned int>(points.size() / dimension)};
}

GLuint Loader::createVao()
{
	GLuint vaoID;
	glGenVertexArrays(1, &vaoID);
	glBindVertexArray(vaoID);
	glEnableVertexAttribArray(0);

	vaos.emplace_back(vaoID);

	return vaoID;
}

void Loader::bindIndexBuffer(const std::vector<unsigned> &indices)
{
	GLuint vboId;
	glGenBuffers(1, &vboId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboId);

	auto size = static_cast<GLsizeiptr>(indices.size() * sizeof(unsigned));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, indices.data(), GL_STATIC_DRAW);

	vbos.emplace_back(vboId);
}

void Loader::storeDataInAttributeList(GLuint attributeNumber, const std::vector<float> &data, GLint coordinateSize)
{
	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	auto size = static_cast<GLsizeiptr>(data.size() * sizeof(float));
	glBufferData(GL_ARRAY_BUFFER, size, data.data(), GL_STATIC_DRAW);

	glVertexAttribPointer(attributeNumber, coordinateSize, GL_FLOAT, GL_FALSE, 0, nullptr);
	vbos.emplace_back(vbo);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	attributes.emplace_back(attributeNumber);
}

void Loader::unbindVao() const
{
	for (auto const &attributeNumber: attributes)
	{
		glDisableVertexAttribArray(attributeNumber);
	}

	glBindVertexArray(0);
}

void Loader::bindVao(const Model &model) const
{
	glBindVertexArray(model.vaoId);

	for (auto const &attributeNumber: attributes)
	{
		glEnableVertexAttribArray(attributeNumber);
	}
}

GLuint Loader::createEmptyVBO(GLsizeiptr vertexCount)
{
	GLuint vbo;
	glGenBuffers(1, &vbo);

	vbos.emplace_back(vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, vertexCount * static_cast<GLsizeiptr>(sizeof(float)), nullptr, GL_STREAM_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	return vbo;
}

void Loader::updateVBO(GLuint vbo, const std::vector<float> &data, GLsizeiptr sizeOfData)
{
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	GLsizeiptr dataSize = sizeOfData * static_cast<GLsizeiptr>(sizeof(float));
	glBufferData(GL_ARRAY_BUFFER, dataSize, data.data(), GL_STREAM_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, dataSize, data.data());

	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Loader::addInstancedAttribute(GLuint vao, GLuint vbo, GLuint attribute, int vertexCount,
								   GLsizei instancedDataLength, int offset)
{
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBindVertexArray(vao);

	const void *pointer = reinterpret_cast<void *>(offset * sizeof(float));
	glVertexAttribPointer(attribute, vertexCount, GL_FLOAT, GL_FALSE,
						  instancedDataLength * static_cast<GLsizei>(sizeof(float)), pointer);

	glVertexAttribDivisor(attribute, 1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

GLuint Loader::loadTexture(const char *texture)
{
	GLuint textureId;
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	loadImage(texture);

	glBindTexture(GL_TEXTURE_2D, 0);

	textures.emplace_back(textureId);
	return textureId;
}

void Loader::loadImage(const char *texture)
{
	stbi_set_flip_vertically_on_load(1);

	int width, height, BPP;
	unsigned char *image = stbi_load(texture, &width, &height, &BPP, 4);

	if (!image)
	{
		throw std::runtime_error("Cannot load image");
	}

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
	stbi_image_free(image);
}

Model::Model(GLuint vaoId, unsigned vertexCount)
		: vaoId(vaoId), vertexCount(vertexCount)
{

}
