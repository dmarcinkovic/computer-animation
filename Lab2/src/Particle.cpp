//
// Created by david on 23. 11. 2021..
//

#include "Particle.h"
#include "Display.h"

bool Particle::update()
{
	static constexpr float GRAVITY = -9.81f;

	auto frameTime = static_cast<float>(Display::getFrameTime());
	velocity.y += GRAVITY * gravityEffect * frameTime;

	glm::vec3 change = velocity * scale;

	position += change;
	elapsedTime += frameTime;

	return elapsedTime < lifeLength;
}

Particle::Particle(GLuint texture, glm::vec3 position, glm::vec3 velocity, float gravityEffect, float lifeLength,
				   float rotation, float scale)
		: texture(texture), position(position), velocity(velocity), gravityEffect(gravityEffect),
		  lifeLength(lifeLength), rotation(rotation), scale(scale)
{

}
