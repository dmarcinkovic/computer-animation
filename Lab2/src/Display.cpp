//
// Created by david on 20. 03. 2020..
//

#include <iostream>
#include "Display.h"
#include "Constants.h"

int Display::windowWidth{};
int Display::windowHeight{};

double Display::m_LastFrameTime{};
double Display::m_DeltaTime{};

Display::Display(int width, int height, const char *title)
{
	windowWidth = width;
	windowHeight = height;

	glfwInit();

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(width, height, title, nullptr, nullptr);
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);

	setViewport(width, height);

	glfwSetWindowSizeCallback(window, windowResizeCallback);
	glewInit();
}

Display::~Display()
{
	glfwDestroyWindow(window);
	glfwTerminate();
}

void Display::update()
{
	double currentTime = glfwGetTime();
	m_DeltaTime = currentTime - m_LastFrameTime;
	m_LastFrameTime = currentTime;

	glfwPollEvents();
	glfwSwapBuffers(window);

	std::cout << "FPS: " << 1.0 / m_DeltaTime << '\n';
}

bool Display::isRunning() const
{
	return !glfwWindowShouldClose(window);
}

void Display::clearWindow()
{
//	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Display::windowResizeCallback(GLFWwindow *, int width, int height)
{
	setViewport(width, height);

	windowWidth = width;
	windowHeight = height;
}

int Display::getWidth()
{
	return windowWidth;
}

int Display::getHeight()
{
	return windowHeight;
}

void Display::setViewport(int width, int height)
{
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	float aspectRatio = static_cast<float>(width) / static_cast<float>(height);
	gluPerspective(Constants::FOV, aspectRatio, Constants::NEAR, Constants::FAR);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

double Display::getFrameTime()
{
	return m_DeltaTime;
}