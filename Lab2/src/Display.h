//
// Created by david on 20. 03. 2020..
//

#ifndef LAB3_DISPLAY_H
#define LAB3_DISPLAY_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <vector>

class Display
{
private:
    GLFWwindow *window;

	static int windowWidth, windowHeight;

	static double m_LastFrameTime;
	static double m_DeltaTime;

public:
    Display(int width, int height, const char *title);

    ~Display();

    void update();

    [[nodiscard]] bool isRunning() const;

    static void clearWindow();

	static int getWidth();

	static int getHeight();

	static double getFrameTime();

private:
	static void windowResizeCallback(GLFWwindow *window, int width, int height);

	static void setViewport(int width, int height);
};


#endif //LAB3_DISPLAY_H
