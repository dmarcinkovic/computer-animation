//
// Created by david on 14. 10. 2021..
//

#ifndef LAB1_UTIL_H
#define LAB1_UTIL_H

#include <string>
#include <vector>

class Util
{
public:
	static std::vector<std::string> split(const std::string &str, const std::string &delimiter);

	static float getRandom(float first = 0.0f, float second = 1.0f);
};


#endif //LAB1_UTIL_H
