
#include "Display.h"
#include "Camera.h"
#include "Particle.h"
#include "ParticleRenderer.h"
#include "ParticleSystem.h"

int main()
{
	Display display(800, 800, "Lab2");

	Camera camera;

	Loader loader;

	std::vector<Particle> particles;
	ParticleRenderer renderer(loader);

	GLuint texture = loader.loadTexture("res/snow.bmp");

//	ParticleSystem particleSystem(texture, 200, 0.3f, 0.05f, 4); // for fire or smoke
	ParticleSystem particleSystem(texture, 200, 0.3f, 0.02f, 200);  // for snow

	while (display.isRunning())
	{
		Display::clearWindow();

		particleSystem.generateParticles(glm::vec3{0, 0, -20.0}, particles);
		ParticleSystem::update(particles);

		renderer.render(particles, camera);

		display.update();
	}

	return 0;
}
