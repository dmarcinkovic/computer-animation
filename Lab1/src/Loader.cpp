#include "Loader.h"

Loader::~Loader()
{
	for (auto &vao: vaos)
	{
		glDeleteVertexArrays(1, &vao);
	}

	for (auto &vbo: vbos)
	{
		glDeleteBuffers(1, &vbo);
	}
}

Model Loader::loadToVao(const std::vector<float> &position, const std::vector<unsigned> &indices)
{
	GLuint vao = createVao();

	bindIndexBuffer(indices);
	storeDataInAttributeList(0, position, 3);

	return {vao, static_cast<unsigned int>(indices.size())};
}

Model Loader::loadToVao(const std::vector<float> &points)
{
	GLuint vao = createVao();

	storeDataInAttributeList(0, points, 3);
	return {vao, static_cast<unsigned int>(points.size() / 3)};
}

GLuint Loader::createVao()
{
	GLuint vaoID;
	glGenVertexArrays(1, &vaoID);
	glBindVertexArray(vaoID);
	glEnableVertexAttribArray(0);

	vaos.emplace_back(vaoID);

	return vaoID;
}

void Loader::bindIndexBuffer(const std::vector<unsigned> &indices)
{
	GLuint vboId;
	glGenBuffers(1, &vboId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboId);

	auto size = static_cast<GLsizeiptr>(indices.size() * sizeof(unsigned));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, indices.data(), GL_STATIC_DRAW);

	vbos.emplace_back(vboId);
}

void Loader::storeDataInAttributeList(GLuint attributeNumber, const std::vector<float> &data, GLint coordinateSize)
{
	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	auto size = static_cast<GLsizeiptr>(data.size() * sizeof(float));
	glBufferData(GL_ARRAY_BUFFER, size, data.data(), GL_STATIC_DRAW);

	glVertexAttribPointer(attributeNumber, coordinateSize, GL_FLOAT, GL_FALSE, 0, nullptr);
	vbos.emplace_back(vbo);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	attributes.emplace_back(attributeNumber);
}

void Loader::unbindVao() const
{
	for (auto const &attributeNumber: attributes)
	{
		glDisableVertexAttribArray(attributeNumber);
	}

	glBindVertexArray(0);
}

void Loader::bindVao(const Model &model) const
{
	glBindVertexArray(model.vaoId);

	for (auto const &attributeNumber: attributes)
	{
		glEnableVertexAttribArray(attributeNumber);
	}
}

Model::Model(GLuint vaoId, unsigned vertexCount)
		: vaoId(vaoId), vertexCount(vertexCount)
{

}
