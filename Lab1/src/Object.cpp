//
// Created by david on 14. 10. 2021..
//

#include "Object.h"
#include "Maths.h"

Object::Object(const Model &model, const glm::vec3 &scale)
		: model(model), scale(scale)
{

}

glm::mat4 Object::getModelMatrix() const
{
	return Maths::createTransformationMatrix(position, angle, rotation, scale);
}

const Model &Object::getModel() const
{
	return model;
}

void Object::setPosition(const glm::vec3 &newPosition)
{
	position = newPosition;
}

void Object::setRotation(float newAngle, const glm::vec3 &newRotation)
{
	angle = newAngle;
	rotation = newRotation;
}
