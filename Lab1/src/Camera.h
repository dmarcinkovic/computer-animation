//
// Created by david on 21. 03. 2020..
//

#ifndef LAB3_CAMERA_H
#define LAB3_CAMERA_H

#include <glm/glm.hpp>

class Camera
{
private:
	glm::vec3 position{};

	float pitch{};
	float yaw{};

	glm::vec3 rotation{};

public:
	[[nodiscard]] const glm::vec3 &getPosition() const;

	[[nodiscard]] float getPitch() const;

	[[nodiscard]] float getYaw() const;

	void mouseScroll(double offset);

	[[nodiscard]] const glm::vec3 &getRotation() const;

	void setPosition(float x, float y, float z);
};


#endif //LAB3_CAMERA_H
