//
// Created by david on 21. 03. 2020..
//

#include <fstream>

#include "ObjLoader.h"
#include "Util.h"

Model ObjLoader::loadObj(Loader &loader, const char *file)
{
    std::ifstream reader(file);

    std::vector<float> vertices;
    std::vector<unsigned > indices;

    std::string line;
    while (std::getline(reader, line))
    {
        if (line[0] == 'v')
        {
            auto points = Util::split(line, " ");
            vertices.emplace_back(std::stof(points[1]));
            vertices.emplace_back(std::stof(points[2]));
            vertices.emplace_back(std::stof(points[3]));
        } else if (line[0] == 'f')
        {
            auto index = Util::split(line, " ");
            indices.emplace_back(std::stoi(index[1]) - 1);
            indices.emplace_back(std::stoi(index[2]) - 1);
            indices.emplace_back(std::stoi(index[3]) - 1);
        }
    }

    return loader.loadToVao(vertices, indices);
}
