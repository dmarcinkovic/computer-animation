//
// Created by david on 21. 03. 2020..
//

#ifndef LAB3_OBJECTSHADER_H
#define LAB3_OBJECTSHADER_H

#include <GL/glew.h>
#include <glm/glm.hpp>

#include "Shader.h"

class ObjectShader : public Shader
{
private:
	constexpr static const char *VERTEX_FILE = "src/shader/VertexShader.glsl";
	constexpr static const char *FRAGMENT_FILE = "src/shader/FragmentShader.glsl";

	GLint locationTransformationMatrix{};
	GLint locationProjectionMatrix{};
	GLint locationViewMatrix{};

public:
	ObjectShader();

	void loadTransformationMatrix(const glm::mat4 &transformationMatrix) const;

	void loadViewMatrix(const glm::mat4 &viewMatrix) const;

	void loadProjectionMatrix(const glm::mat4 &projectionMatrix) const;

private:
	void getUniformLocations();
};


#endif //LAB3_OBJECTSHADER_H
