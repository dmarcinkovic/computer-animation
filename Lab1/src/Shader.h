//
// Created by david on 24. 04. 2020..
//

#ifndef LAB3_SHADER_H
#define LAB3_SHADER_H

#include <GL/glew.h>
#include <glm/glm.hpp>

class Shader
{
private:
	GLuint vertexShader;
	GLuint fragmentShader;
	GLuint program;

	static GLuint loadShader(const char *filename, GLenum type);

	static void debug(GLuint shaderId);

protected:
	GLint getUniformLocation(const char *name) const;

public:
	Shader(const char *vertexFile, const char *fragmentFile);

	virtual ~Shader();

	void start() const;

	static void stop();
};


#endif //LAB3_SHADER_H
