//
// Created by david on 14. 10. 2021..
//

#ifndef LAB1_OBJECT_H
#define LAB1_OBJECT_H

#include <glm/glm.hpp>

#include "Loader.h"

class Object
{
private:
	Model model;

	glm::vec3 position{};
	glm::vec3 rotation{};
	glm::vec3 scale;

	float angle{};

public:
	Object(const Model &model, const glm::vec3 &scale);

	[[nodiscard]] glm::mat4 getModelMatrix() const;

	[[nodiscard]] const Model &getModel() const;

	void setPosition(const glm::vec3 &newPosition);

	void setRotation(float newAngle, const glm::vec3 &newRotation);

};


#endif //LAB1_OBJECT_H
