//
// Created by david on 14. 10. 2021..
//

#ifndef LAB1_OBJECTRENDERER_H
#define LAB1_OBJECTRENDERER_H

#include <vector>
#include <functional>

#include "Object.h"
#include "ObjectShader.h"
#include "Camera.h"

class ObjectRenderer
{
private:
	std::vector<std::reference_wrapper<Object>> objects;
	ObjectShader objectShader;

public:
	void render(const Loader &loader, const Camera &camera) const;

	void addObject(Object &object);

private:
	void prepareRendering() const;

	static void finishRendering();

	void renderObject(const Object &object, const Loader &loader) const;
};


#endif //LAB1_OBJECTRENDERER_H
