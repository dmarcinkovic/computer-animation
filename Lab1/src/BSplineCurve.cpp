//
// Created by david on 14. 10. 2021..
//

#include <iostream>
#include <fstream>
#include <GL/gl.h>

#include "BSplineCurve.h"
#include "Util.h"

std::vector<glm::vec3> BSplineCurve::calcPoints(const std::vector<glm::vec3> &points)
{
	std::vector<glm::vec3> resultPoints;

	constexpr float C = 1.0f / 6.0f;
	constexpr glm::mat4 B{-1, 3, -3, 1, 3, -6, 3, 0, -3, 0, 3, 0, 1, 4, 1, 0};

	for (int i = 0; i < points.size() - 3UL; ++i)
	{
		glm::vec3 p1 = points[i];
		glm::vec3 p2 = points[i + 1];
		glm::vec3 p3 = points[i + 2];
		glm::vec3 p4 = points[i + 3];

		for (float t = 0; t < 1; t += 0.01f)
		{
			glm::vec4 T{t * t * t, t * t, t, 1};
			glm::mat4x3 R{p1, p2, p3, p4};

			glm::vec3 p = C * R * B * T;
			resultPoints.emplace_back(p);
		}
	}

	return resultPoints;
}

std::vector<glm::vec3> BSplineCurve::calcTangents(const std::vector<glm::vec3> &points)
{
	std::vector<glm::vec3> resultTangents;

	constexpr float C = 1.0f / 2.0f;
	constexpr glm::mat3x4 B{-1, 3, -3, 1, 2, -4, 2, 0, -1, 0, 1, 0};

	for (int i = 0; i < points.size() - 3UL; ++i)
	{
		glm::vec3 p1 = points[i];
		glm::vec3 p2 = points[i + 1];
		glm::vec3 p3 = points[i + 2];
		glm::vec3 p4 = points[i + 3];

		for (float t = 0; t < 1; t += 0.01f)
		{
			glm::vec3 T{t * t, t, 1};
			glm::mat4x3 R{p1, p2, p3, p4};

			glm::vec3 p = C * R * B * T;
			resultTangents.emplace_back(p);
		}
	}

	return resultTangents;
}

std::vector<glm::vec3> BSplineCurve::calcSecondDerivative(const std::vector<glm::vec3> &points)
{
	std::vector<glm::vec3> result;

	constexpr glm::mat2x4 B{-1, 3, -3, 1, 1, -2, 1, 0};

	for (int i = 0; i < points.size() - 3UL; ++i)
	{
		glm::vec3 p1 = points[i];
		glm::vec3 p2 = points[i + 1];
		glm::vec3 p3 = points[i + 2];
		glm::vec3 p4 = points[i + 3];

		for (float t = 0; t < 1; t += 0.01f)
		{
			glm::vec2 T{t, 1};
			glm::mat4x3 R{p1, p2, p3, p4};

			glm::vec3 p = R * B * T;
			result.emplace_back(p);
		}
	}

	return result;
}

std::vector<glm::vec3> BSplineCurve::loadPointsFromFile(const char *filename)
{
	std::ifstream reader(filename);

	if (!reader)
	{
		throw std::runtime_error("Cannot open file");
	}

	std::vector<glm::vec3> points;
	std::string line;
	while (std::getline(reader, line))
	{
		std::vector<std::string> point = Util::split(line, " ");
		float x = std::stof(point[0]) / 10.0f;
		float y = std::stof(point[1]) / 10.0f;
		float z = std::stof(point[2]) / 10.0f;

		points.emplace_back(x, y, -z);
	}

	return points;
}

void BSplineCurve::drawPoints(const glm::mat4 &viewMatrix, const std::vector<glm::vec3> &points, const glm::vec3 &color)
{
	glColor3f(color.x, color.y, color.z);

	glBegin(GL_LINE_STRIP);
	for (const glm::vec3 &point: points)
	{
		glm::vec4 newPoint = viewMatrix * glm::vec4{point.x, point.y, point.z, 1.0f};
		glVertex3f(newPoint.x, newPoint.y, newPoint.z);
	}

	glEnd();
}

void BSplineCurve::drawTangent(const glm::mat4 &viewMatrix, const glm::vec3 &linePoint, const glm::vec3 &tangent,
							   const glm::vec3 &secondDerivative)
{
	static constexpr float scale = 0.015f;

	glLineWidth(5.0f);
	glBegin(GL_LINES);

	glm::vec3 point2 = linePoint + tangent;

	glm::vec4 firstPoint = viewMatrix * glm::vec4{linePoint.x, linePoint.y, linePoint.z, 1.0f};
	glm::vec4 secondPoint = scale * viewMatrix * glm::vec4{point2.x, point2.y, point2.z, 1.0f};

	glColor3f(0, 0, 1);
	glVertex3f(firstPoint.x, firstPoint.y, firstPoint.z);
	glVertex3f(secondPoint.x, secondPoint.y, secondPoint.z);

	glm::vec3 point3 = linePoint + glm::cross(tangent, secondDerivative);
	glm::vec4 thirdPoint = scale * viewMatrix * glm::vec4{point3.x, point3.y, point3.z, 1.0f};

	glColor3f(0, 1, 0);
	glVertex3f(firstPoint.x, firstPoint.y, firstPoint.z);
	glVertex3f(thirdPoint.x, thirdPoint.y, thirdPoint.z);

	glm::vec3 point4 = linePoint + glm::cross(tangent, glm::cross(tangent, secondDerivative));
	glm::vec4 fourthPoint = scale * viewMatrix * glm::vec4{point4.x, point4.y, point4.z, 1.0f};

	glColor3f(1, 0, 0);
	glVertex3f(firstPoint.x, firstPoint.y, firstPoint.z);
	glVertex3f(fourthPoint.x, fourthPoint.y, fourthPoint.z);

	glEnd();
	glLineWidth(1.0f);
}
