//
// Created by david on 21. 03. 2020..
//

#ifndef LAB3_OBJLOADER_H
#define LAB3_OBJLOADER_H

#include <vector>
#include <string>

#include "Loader.h"

class ObjLoader
{
public:
	static Model loadObj(Loader &loader, const char *file);
};


#endif //LAB3_OBJLOADER_H
