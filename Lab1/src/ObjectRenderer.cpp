//
// Created by david on 14. 10. 2021..
//

#include "ObjectRenderer.h"
#include "Maths.h"
#include "Display.h"
#include "Constants.h"

void ObjectRenderer::render(const Loader &loader, const Camera &camera) const
{
	prepareRendering();

	for (auto const &object: objects)
	{
		glm::mat4 viewMatrix = Maths::createViewMatrix(camera);
		objectShader.loadViewMatrix(viewMatrix);

		renderObject(object.get(), loader);

		loader.unbindVao();
	}

	finishRendering();
}

void ObjectRenderer::addObject(Object &object)
{
	objects.emplace_back(object);
}

void ObjectRenderer::prepareRendering() const
{
	objectShader.start();

	auto width = static_cast<float>(Display::getWidth());
	auto height = static_cast<float>(Display::getHeight());

	float fov = Constants::FOV;
	float far = Constants::FAR;
	float near = Constants::NEAR;
	glm::mat4 projectionMatrix = Maths::createProjectionMatrix(fov, far, near, width, height);
	objectShader.loadProjectionMatrix(projectionMatrix);
}

void ObjectRenderer::finishRendering()
{
	ObjectShader::stop();
}

void ObjectRenderer::renderObject(const Object &object, const Loader &loader) const
{
	loader.bindVao(object.getModel());

	glm::mat4 modelMatrix = object.getModelMatrix();
	objectShader.loadTransformationMatrix(modelMatrix);

	auto count = static_cast<GLsizei>(object.getModel().vertexCount);
	glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_INT, nullptr);
}
