//
// Created by david on 21. 03. 2020..
//

#include "ObjectShader.h"

ObjectShader::ObjectShader()
    : Shader(VERTEX_FILE, FRAGMENT_FILE)
{
    getUniformLocations();
}

void ObjectShader::getUniformLocations()
{
    locationTransformationMatrix = getUniformLocation("transformationMatrix");
    locationProjectionMatrix = getUniformLocation("projectionMatrix");
    locationViewMatrix = getUniformLocation("viewMatrix");
}

void ObjectShader::loadTransformationMatrix(const glm::mat4 &transformationMatrix) const
{
    glUniformMatrix4fv(locationTransformationMatrix, 1, GL_FALSE, &transformationMatrix[0][0]);
}

void ObjectShader::loadViewMatrix(const glm::mat4 &viewMatrix) const
{
    glUniformMatrix4fv(locationViewMatrix, 1, GL_FALSE, &viewMatrix[0][0]);
}

void ObjectShader::loadProjectionMatrix(const glm::mat4 &projectionMatrix) const
{
    glUniformMatrix4fv(locationProjectionMatrix, 1, GL_FALSE, &projectionMatrix[0][0]);
}
