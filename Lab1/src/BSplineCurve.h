//
// Created by david on 14. 10. 2021..
//

#ifndef LAB1_BSPLINECURVE_H
#define LAB1_BSPLINECURVE_H

#include <vector>
#include <glm/glm.hpp>

class BSplineCurve
{
public:
	static std::vector<glm::vec3> calcPoints(const std::vector<glm::vec3> &points);

	static std::vector<glm::vec3> calcTangents(const std::vector<glm::vec3> &points);

	static std::vector<glm::vec3> calcSecondDerivative(const std::vector<glm::vec3> &points);

	static std::vector<glm::vec3> loadPointsFromFile(const char *filename);

	static void drawPoints(const glm::mat4 &viewMatrix, const std::vector<glm::vec3> &points, const glm::vec3 &color);

	static void drawTangent(const glm::mat4 &viewMatrix, const glm::vec3 &linePoint,
							const glm::vec3 &tangent, const glm::vec3 &secondDerivative);
};


#endif //LAB1_BSPLINECURVE_H
