//
// Created by david on 20. 03. 2020..
//

#ifndef LAB3_DISPLAY_H
#define LAB3_DISPLAY_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <vector>

class Display
{
private:
    GLFWwindow *window;

	static int windowWidth, windowHeight;

public:
    Display(int width, int height, const char *title);

    ~Display();

    void update();

    [[nodiscard]] bool isRunning() const;

    static void clearWindow();

	static int getWidth();

	static int getHeight();

private:
	static void windowResizeCallback(GLFWwindow *window, int width, int height);

	static void setViewport(int width, int height);
};


#endif //LAB3_DISPLAY_H
