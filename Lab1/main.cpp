#include "src/Display.h"
#include "src/Loader.h"
#include "src/ObjLoader.h"
#include "src/Maths.h"
#include "src/Object.h"
#include "src/ObjectRenderer.h"
#include "src/BSplineCurve.h"

int main()
{
	constexpr int width = 800;
	constexpr int height = 800;

	Display display(width, height, "Lab1");

	Loader loader;

	Model model = ObjLoader::loadObj(loader, "res/f16.obj");
	Object plane(model, glm::vec3{0.7f});

	ObjectRenderer renderer;
	renderer.addObject(plane);

	Camera camera;
	camera.setPosition(0.5, 0.4, 0.5);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	std::vector<glm::vec3> points = BSplineCurve::loadPointsFromFile("res/vertices.txt");
	std::vector<glm::vec3> splinePoints = BSplineCurve::calcPoints(points);
	std::vector<glm::vec3> splineTangents = BSplineCurve::calcTangents(points);
	std::vector<glm::vec3> splineSecondDerivative = BSplineCurve::calcSecondDerivative(points);

	int counter = 0;

	while (display.isRunning())
	{
		Display::clearWindow();

		if (counter < splinePoints.size())
		{
			plane.setPosition(splinePoints[counter]);

			glm::vec3 e = splineTangents[counter];
			glm::vec3 s{0, 0, 1};
			glm::vec3 os = glm::cross(s, e);

			double cosPhi = glm::dot(s, e) / (glm::length(s) * glm::length(e));
			double phi = acos(cosPhi);
			float angle = glm::degrees(static_cast<float>(phi));
			plane.setRotation(angle, os);

			++counter;
		}

		glm::mat4 viewMatrix = Maths::createViewMatrix(camera);
		glm::mat4 transformationMatrix = plane.getModelMatrix();

		BSplineCurve::drawPoints(viewMatrix, splinePoints, glm::vec3{0, 0, 0});
		BSplineCurve::drawTangent(viewMatrix, splinePoints[counter], splineTangents[counter],
								  splineSecondDerivative[counter]);

		renderer.render(loader, camera);

		display.update();
	}

	return 0;
}
