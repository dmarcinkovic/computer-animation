#include "src/Display.h"
#include "src/Camera.h"
#include "src/Loader.h"
#include "src/Object.h"
#include "src/ObjParser.h"
#include "src/ObjectRenderer.h"
#include "src/Light.h"
#include "src/PhysicsEngine.h"
#include "src/Editor.h"

int main()
{
	Display display(1400, 800, "Project");

	Camera camera;
	Light light{glm::vec3{1, 1, 1}, glm::vec3{2.0f}};

	Loader loader;

	GLuint texture = loader.loadTexture("assets/ball.png");
	GLuint normalTexture = loader.loadTexture("assets/ball_normal.png");

	Model sphereModel = ObjParser::loadObj("assets/soccer.obj", loader);
	Object sphere(sphereModel, texture, normalTexture);
	sphere.position = glm::vec3{1, 0, -10};

	Object sphere2(sphereModel, texture, normalTexture);
	sphere2.position = glm::vec3{0, 2, -10};

	Object sphere3(sphereModel, texture, normalTexture);
	sphere3.position = glm::vec3{-1, 4, -10};

	Object sphere4(sphereModel, texture, normalTexture);
	sphere4.position = glm::vec3{-2, 0, -10};

	Object sphere5(sphereModel, texture, normalTexture);
	sphere5.position = glm::vec3{2, 3, -10};

	Model cubeModel = ObjParser::loadObj("assets/cube2.obj", loader);
	GLuint brickTexture = loader.loadTexture("assets/brick.jpg");
	GLuint brickNormalTexture = loader.loadTexture("assets/brick_normal.jpg");
	Object platform(cubeModel, brickTexture, brickNormalTexture);
	platform.position = glm::vec3{0, -3, -10};
	platform.scale = glm::vec3{4, 0.2, 3};

	ObjectRenderer renderer;
	renderer.addObject(platform);
	renderer.addObject(sphere);
	renderer.addObject(sphere2);
	renderer.addObject(sphere3);
	renderer.addObject(sphere4);
	renderer.addObject(sphere5);

	PhysicsEngine engine(0.3f);
	engine.addPlatform(platform);
	engine.addSphere(sphere);
	engine.addSphere(sphere2);
	engine.addSphere(sphere3);
	engine.addSphere(sphere4);
	engine.addSphere(sphere5);

	bool play{};

	while (display.isRunning())
	{
		Display::clearWindow();

		Editor::newFrame();
		Editor::draw(renderer, camera, play);
		Editor::render();

		renderer.render(loader, camera, light);

		if (play)
		{
			float delta = display.getDeltaTime();
			engine.update(delta);
		}

		display.update();
	}

	return 0;
}
