#version 450 core

out vec4 outColor;

in vec3 surfaceNormal;
in vec4 worldPosition;
in vec2 textCoordinates;

uniform vec3 lightColor;
uniform vec3 lightPosition;
uniform vec3 cameraPosition;

uniform float shineDamper;
uniform int material;

uniform sampler2D objectTexture;

void main()
{
    // Ambient component
    const float ambientFactor = 0.2;
    vec3 ambient = lightColor * ambientFactor;

    vec3 lightDirection = normalize(lightPosition - worldPosition.xyz);

    // Diffuse component
    float diffuseFactor = max(dot(surfaceNormal, lightDirection), 0.0);
    vec3 diffuse = lightColor * diffuseFactor;

    // Specular component
    vec3 toCameraVector = normalize(cameraPosition - worldPosition.xyz);
    vec3 reflectedVector = reflect(-lightDirection, surfaceNormal);

    float specularFactor = pow(max(dot(reflectedVector, toCameraVector), 0.0), material);
    vec3 specular = specularFactor * shineDamper * lightColor;

    vec4 color = texture(objectTexture, textCoordinates);
    vec3 totalColor = (diffuse + ambient) * color.rgb + specular;

    outColor = vec4(totalColor, 1);
}
