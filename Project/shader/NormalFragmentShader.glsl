#version 450 core

out vec4 outColor;

in vec4 worldPosition;
in vec2 textCoordinates;
in vec3 lightPos;
in vec3 cameraPos;
in vec3 tangentFragPos;

uniform vec3 lightColor;
uniform float shineDamper;
uniform int material;

uniform sampler2D objectTexture;
uniform sampler2D normalMap;

void main()
{
    vec3 normal = texture(normalMap, textCoordinates).rgb;
    normal = normalize(normal * 2.0 - 1.0);

    // Ambient component
    const float ambientFactor = 0.2;
    vec3 ambient = lightColor * ambientFactor;

    vec3 lightDirection = normalize(lightPos - tangentFragPos);

    // Diffuse component
    float diffuseFactor = max(dot(normal, lightDirection), 0.0);
    vec3 diffuse = lightColor * diffuseFactor;

    // Specular component
    vec3 toCameraVector = normalize(cameraPos - tangentFragPos);
    vec3 reflectedVector = reflect(-lightDirection, normal);

    float specularFactor = pow(max(dot(reflectedVector, toCameraVector), 0.0), material);
    vec3 specular = specularFactor * shineDamper * lightColor;

    vec4 color = texture(objectTexture, textCoordinates);
    vec3 totalColor = (diffuse + ambient) * color.rgb + specular;

    outColor = vec4(totalColor, 1);
}
