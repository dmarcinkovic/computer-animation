#version 450 core

out vec4 outColor;

in vec4 worldPosition;
in vec2 textCoordinates;
in vec3 lightPos;
in vec3 cameraPos;
in vec3 tangentFragPos;
in vec3 surfaceNormal;

uniform vec3 lightPosition;
uniform vec3 cameraPosition;
uniform vec3 lightColor;
uniform float shineDamper;
uniform int material;

uniform sampler2D objectTexture;
uniform sampler2D normalMap;

uniform int useNormal;

void main()
{
    vec3 normal = texture(normalMap, textCoordinates).rgb;
    normal = normalize(normal * 2.0 - 1.0);

    // Ambient component
    const float ambientFactor = 0.2;
    vec3 ambient = lightColor * ambientFactor;

    vec3 lightDirection;
    float diffuseFactor;
    if (useNormal == 1)
    {
        lightDirection = normalize(lightPos - tangentFragPos);
        diffuseFactor = max(dot(normal, lightDirection), 0.0);
    } else
    {
        lightDirection = normalize(lightPosition - worldPosition.xyz);
        diffuseFactor = max(dot(surfaceNormal, lightDirection), 0.0);
    }

    // Diffuse component
    vec3 diffuse = lightColor * diffuseFactor;

    // Specular component
    vec3 toCameraVector;
    vec3 reflectedVector;

    if (useNormal == 1)
    {
        toCameraVector = normalize(cameraPos - tangentFragPos);
        reflectedVector = reflect(-lightDirection, normal);
    } else
    {
        toCameraVector = normalize(cameraPosition - worldPosition.xyz);
        reflectedVector = reflect(-lightDirection, surfaceNormal);
    }

    float specularFactor = pow(max(dot(reflectedVector, toCameraVector), 0.0), material);
    vec3 specular = specularFactor * shineDamper * lightColor;

    vec4 color = texture(objectTexture, textCoordinates);
    vec3 totalColor = (diffuse + ambient) * color.rgb + specular;

    outColor = vec4(totalColor, 1);
}
