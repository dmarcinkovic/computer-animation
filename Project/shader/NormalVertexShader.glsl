#version 450 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 textureCoordinates;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec3 tangent;

uniform mat4 transformationMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform vec3 lightPosition;
uniform vec3 cameraPosition;

out vec4 worldPosition;
out vec2 textCoordinates;
out vec3 lightPos;
out vec3 cameraPos;
out vec3 tangentFragPos;

void main()
{
    worldPosition = transformationMatrix * vec4(position, 1.0f);
    gl_Position = projectionMatrix * viewMatrix * worldPosition;

    mat3 normalMatrix = transpose(inverse(mat3(transformationMatrix)));
    vec3 T = normalize(normalMatrix * tangent);
    vec3 N = normalize(normalMatrix * normal);
    vec3 B = cross(N, T);

    mat3 TBN = transpose(mat3(T, B, N));
    lightPos = TBN * lightPosition;
    cameraPos = TBN * cameraPosition;
    tangentFragPos = TBN * worldPosition.xyz;

    textCoordinates = textureCoordinates;
}
