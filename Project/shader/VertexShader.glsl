#version 450 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 textureCoordinates;
layout (location = 2) in vec3 normal;

uniform mat4 transformationMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

out vec3 surfaceNormal;
out vec4 worldPosition;
out vec2 textCoordinates;

void main()
{
    worldPosition = transformationMatrix * vec4(position, 1.0f);
    gl_Position = projectionMatrix * viewMatrix * worldPosition;

    surfaceNormal = mat3(transpose(inverse(transformationMatrix))) * normal;
    surfaceNormal = normalize(surfaceNormal);

    textCoordinates = textureCoordinates;
}
