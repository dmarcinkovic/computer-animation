#include <string>
#include <iostream>
#include <glm/glm.hpp>

#include "stb_image.h"
#include "Loader.h"

Loader::~Loader()
{
	glDeleteTextures(static_cast<GLsizei>(textureIds.size()), textureIds.data());
	glDeleteVertexArrays(static_cast<GLsizei>(vaos.size()), vaos.data());
	glDeleteBuffers(static_cast<GLsizei>(vbos.size()), vbos.data());
}

Model Loader::loadToVao(const std::vector<float> &position, const std::vector<float> &textures,
						const std::vector<float> &normals)
{
	GLuint vao = createVao();

	storeDataInAttributeList(0, position, 3);
	storeDataInAttributeList(1, textures, 2);
	storeDataInAttributeList(2, normals, 3);

	return {vao, static_cast<unsigned int>(position.size() / 3)};
}

Model Loader::loadToVao(const std::vector<float> &position, const std::vector<float> &textures,
						const std::vector<float> &normals, const std::vector<float> &tangents)
{
	GLuint vao = createVao();

	storeDataInAttributeList(0, position, 3);
	storeDataInAttributeList(1, textures, 2);
	storeDataInAttributeList(2, normals, 3);
	storeDataInAttributeList(3, tangents, 3);

	return {vao, static_cast<unsigned int>(position.size() / 3)};
}

GLuint Loader::createVao()
{
	GLuint vaoID;

	glGenVertexArrays(1, &vaoID);
	glBindVertexArray(vaoID);
	glEnableVertexAttribArray(0);

	vaos.emplace_back(vaoID);

	return vaoID;
}

void Loader::bindIndexBuffer(const std::vector<unsigned> &indices)
{
	GLuint vboId;
	glGenBuffers(1, &vboId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboId);

	auto size = static_cast<GLsizeiptr>(indices.size() * sizeof(unsigned));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, indices.data(), GL_STATIC_DRAW);

	vbos.emplace_back(vboId);
}

void Loader::storeDataInAttributeList(GLuint attributeNumber, const std::vector<float> &data, GLint coordinateSize)
{
	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	auto size = static_cast<GLsizeiptr>(data.size() * sizeof(float));
	glBufferData(GL_ARRAY_BUFFER, size, data.data(), GL_STATIC_DRAW);

	glVertexAttribPointer(attributeNumber, coordinateSize, GL_FLOAT, GL_FALSE, 0, nullptr);
	vbos.emplace_back(vbo);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	attributes.emplace_back(attributeNumber);
}

void Loader::unbindVao() const
{
	for (auto const &attributeNumber: attributes)
	{
		glDisableVertexAttribArray(attributeNumber);
	}

	glBindVertexArray(0);
}

void Loader::bindVao(const Model &model) const
{
	glBindVertexArray(model.vaoId);

	for (auto const &attributeNumber: attributes)
	{
		glEnableVertexAttribArray(attributeNumber);
	}
}

GLuint Loader::loadTexture(const char *texture)
{
	GLuint textureId;
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	bool loaded = loadImage(texture);
	if (!loaded)
	{
		textureId = 0;
	}

	glBindTexture(GL_TEXTURE_2D, 0);

	textureIds.emplace_back(textureId);
	return textureId;
}

bool Loader::loadImage(const char *texture)
{
	stbi_set_flip_vertically_on_load(1);

	int width, height, BPP;
	unsigned char *image = stbi_load(texture, &width, &height, &BPP, 4);

	if (!image)
	{
		std::string message = "Error while loading " + std::string(texture);
		std::cout << message << '\n';

		return false;
	}

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
	stbi_image_free(image);

	return true;
}

Model Loader::renderQuad()
{
	glm::vec3 pos1(-1.0f, 1.0f, 0.0f);
	glm::vec3 pos2(-1.0f, -1.0f, 0.0f);
	glm::vec3 pos3(1.0f, -1.0f, 0.0f);
	glm::vec3 pos4(1.0f, 1.0f, 0.0f);

	glm::vec2 uv1(0.0f, 1.0f);
	glm::vec2 uv2(0.0f, 0.0f);
	glm::vec2 uv3(1.0f, 0.0f);
	glm::vec2 uv4(1.0f, 1.0f);

	glm::vec3 tangent1 = calcTangent(pos1, pos2, pos3, uv1, uv2, uv3);
	glm::vec3 tangent2 = calcTangent(pos1, pos3, pos4, uv1, uv3, uv4);

	std::vector<float> positions{pos1.x, pos1.y, pos1.z, pos2.x, pos2.y, pos2.z, pos3.x, pos3.y, pos3.z,
								 pos1.x, pos1.y, pos1.z, pos3.x, pos3.y, pos3.z, pos4.x, pos4.y, pos4.z};
	std::vector<float> textures{uv1.x, uv1.y, uv2.x, uv2.y, uv3.x, uv3.y,
								uv1.x, uv1.y, uv3.x, uv3.y, uv4.x, uv4.y};
	std::vector<float> normals{0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
							   0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f};
	std::vector<float> tangents{tangent1.x, tangent1.y, tangent1.z,
								tangent1.x, tangent1.y, tangent1.z,
								tangent1.x, tangent1.y, tangent1.z,
								tangent2.x, tangent2.y, tangent2.z,
								tangent2.x, tangent2.y, tangent2.z,
								tangent2.x, tangent2.y, tangent2.z};

	return loadToVao(positions, textures, normals, tangents);
}

glm::vec3 Loader::calcTangent(const glm::vec3 &pos1, const glm::vec3 &pos2, const glm::vec3 &pos3, const glm::vec2 &uv1,
							  const glm::vec2 &uv2, const glm::vec2 &uv3)
{
	glm::vec3 edge1 = pos2 - pos1;
	glm::vec3 edge2 = pos3 - pos1;

	glm::vec2 deltaUV1 = uv2 - uv1;
	glm::vec2 deltaUV2 = uv3 - uv1;

	float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

	glm::vec3 tangent;
	tangent.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
	tangent.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
	tangent.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);

	return tangent;
}

Model::Model(GLuint vaoId, unsigned vertexCount)
		: vaoId(vaoId), vertexCount(vertexCount)
{

}
