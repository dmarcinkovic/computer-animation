//
// Created by david on 21. 03. 2020..
//

#include "ObjectShader.h"

ObjectShader::ObjectShader()
    : Shader(VERTEX_FILE, FRAGMENT_FILE)
{
    getUniformLocations();
}

void ObjectShader::getUniformLocations()
{
    locationTransformationMatrix = getUniformLocation("transformationMatrix");
    locationProjectionMatrix = getUniformLocation("projectionMatrix");
    locationViewMatrix = getUniformLocation("viewMatrix");

	locationLightPosition = getUniformLocation("lightPosition");
	locationLightColor = getUniformLocation("lightColor");
	locationShineDamper = getUniformLocation("shineDamper");
	locationMaterial = getUniformLocation("material");
	locationCameraPosition = getUniformLocation("cameraPosition");

	locationObjectTexture = getUniformLocation("objectTexture");
	locationNormalMapTexture = getUniformLocation("normalMap");

	locationUseNormal = getUniformLocation("useNormal");
}

void ObjectShader::loadTransformationMatrix(const glm::mat4 &transformationMatrix) const
{
    glUniformMatrix4fv(locationTransformationMatrix, 1, GL_FALSE, &transformationMatrix[0][0]);
}

void ObjectShader::loadViewMatrix(const glm::mat4 &viewMatrix) const
{
    glUniformMatrix4fv(locationViewMatrix, 1, GL_FALSE, &viewMatrix[0][0]);
}

void ObjectShader::loadProjectionMatrix(const glm::mat4 &projectionMatrix) const
{
    glUniformMatrix4fv(locationProjectionMatrix, 1, GL_FALSE, &projectionMatrix[0][0]);
}

void ObjectShader::loadLight(const glm::vec3 &lightPosition, const glm::vec3 &lightColor, float shineDamper,
							 int material) const
{
	glUniform3f(locationLightPosition, lightPosition.x, lightPosition.y, lightPosition.z);
	glUniform3f(locationLightColor, lightColor.x, lightColor.y, lightColor.z);
	glUniform1f(locationShineDamper, shineDamper);
	glUniform1i(locationMaterial, material);
}

void ObjectShader::loadCameraPosition(const glm::vec3 &cameraPosition) const
{
	glUniform3f(locationCameraPosition, cameraPosition.x, cameraPosition.y, cameraPosition.z);
}

void ObjectShader::loadObjectTexture() const
{
	glUniform1i(locationObjectTexture, 0);
	glUniform1i(locationNormalMapTexture, 1);
}

void ObjectShader::loadUseNormal(bool useNormal) const
{
	glUniform1i(locationUseNormal, useNormal);
}
