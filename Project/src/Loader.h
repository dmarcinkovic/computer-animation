//
// Created by david on 21. 03. 2020..
//

#ifndef LAB3_LOADER_H
#define LAB3_LOADER_H

#include <vector>
#include <GL/glew.h>

struct Model
{
	GLuint vaoId;
	unsigned int vertexCount;

	Model(GLuint vaoId, unsigned vertexCount);

	Model() = default;
};

class Loader
{
private:
	std::vector<GLuint> vbos;
	std::vector<GLuint> vaos;
	std::vector<GLuint> attributes;
	std::vector<GLuint> textureIds;

	GLuint createVao();

	void bindIndexBuffer(const std::vector<unsigned> &indices);

	void storeDataInAttributeList(GLuint attributeNumber, const std::vector<float> &data, GLint coordinateSize);

	static bool loadImage(const char *texture);

	static glm::vec3 calcTangent(const glm::vec3 &pos1, const glm::vec3 &pos2, const glm::vec3 &pos3,
						  const glm::vec2 &uv1, const glm::vec2 &uv2, const glm::vec2 &uv3);

public:
	~Loader();

	Model loadToVao(const std::vector<float> &position, const std::vector<float> &textures,
					const std::vector<float> &normals);

	Model loadToVao(const std::vector<float> &position, const std::vector<float> &textures,
					const std::vector<float> &normals, const std::vector<float> &tangents);

	void unbindVao() const;

	void bindVao(const Model &model) const;

	GLuint loadTexture(const char *texture);

	Model renderQuad();
};


#endif //LAB3_LOADER_H
