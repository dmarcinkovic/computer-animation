//
// Created by david on 21. 03. 2020..

#include "Camera.h"
#include "Display.h"
#include "Editor.h"

Camera::Camera()
{
	Display::addScrollListener([this](double xOffset, double yOffset) {
		if (!Editor::isEditorHovered())
		{
			position.z -= static_cast<float>(yOffset);
		}
	});
}

const glm::vec3 &Camera::getPosition() const
{
	return position;
}

float Camera::getPitch() const
{
	return pitch;
}

float Camera::getYaw() const
{
	return yaw;
}

void Camera::mouseScroll(double offset)
{
	if (offset > 0)
	{
		position.z -= 0.1f;
	} else
	{
		position.z += 0.1f;
	}
}

void Camera::setPosition(float x, float y, float z)
{
	position.x = x;
	position.y = y;
	position.z = z;
}

glm::vec3 &Camera::getPosition()
{
	return position;
}

float &Camera::getYaw()
{
	return yaw;
}

float &Camera::getPitch()
{
	return pitch;
}
