//
// Created by david on 26. 12. 2021..
//

#ifndef PROJEKT_LIGHT_H
#define PROJEKT_LIGHT_H

#include <glm/glm.hpp>

struct Light
{
	glm::vec3 color{1.0f};
	glm::vec3 position{0.0f};
};

#endif //PROJEKT_LIGHT_H
