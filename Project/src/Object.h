//
// Created by david on 14. 10. 2021..
//

#ifndef LAB1_OBJECT_H
#define LAB1_OBJECT_H

#include <glm/glm.hpp>

#include "Loader.h"

class Object
{
private:
	friend class PhysicsEngine;

	Model model{};
	GLuint texture{};

	GLuint normalMapTexture{};

public:
	glm::vec3 position{};
	glm::vec3 rotation{};
	glm::vec3 scale{1.0f};

	glm::vec3 velocity{};

	float restitution = 0.4f;
	float friction = 0.1f;

	float mass = 1.0f;

	float shineDamper = 0.4f;
	int material = 2;

	bool useNormalMap{};

public:
	explicit Object(const Model &model, GLuint texture, GLuint normalTexture);

	Object() = default;

	[[nodiscard]] glm::mat4 getModelMatrix() const;

	[[nodiscard]] const Model &getModel() const;

	void bindTexture() const;

	void bindNormalMapTexture() const;

	static void unbindTexture();
};


#endif //LAB1_OBJECT_H
