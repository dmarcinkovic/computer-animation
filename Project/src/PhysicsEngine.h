//
// Created by david on 26. 12. 2021..
//

#ifndef PROJEKT_PHYSICSENGINE_H
#define PROJEKT_PHYSICSENGINE_H

#include <vector>
#include <functional>

#include "Object.h"

class PhysicsEngine
{
private:
	float gravity;

	std::vector<std::reference_wrapper<Object>> spheres;
	Object platform;

public:
	explicit PhysicsEngine(float gravity);

	void update(float delta);

	void addSphere(Object &sphere);

	void addPlatform(const Object &object);

private:
	void collideSpheres();

	void collideSpherePlatform();
};


#endif //PROJEKT_PHYSICSENGINE_H
