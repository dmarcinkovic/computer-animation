//
// Created by david on 26. 12. 2021..
//

#include <iostream>
#include "PhysicsEngine.h"

PhysicsEngine::PhysicsEngine(float gravity)
		: gravity(gravity)
{

}

void PhysicsEngine::update(float delta)
{
	if (delta == 0)
	{
		return;
	}

	for (auto &body: spheres)
	{
		Object &sphere = body.get();

		sphere.velocity.y += delta * (-gravity);

		sphere.position += sphere.velocity;
	}

	collideSpheres();
	collideSpherePlatform();
}

void PhysicsEngine::addSphere(Object &sphere)
{
	spheres.emplace_back(sphere);
}

void PhysicsEngine::addPlatform(const Object &object)
{
	platform = object;
}

void PhysicsEngine::collideSpheres()
{
	for (int i = 0; i < spheres.size(); ++i)
	{
		for (int j = i + 1; j < spheres.size(); ++j)
		{
			Object &sphere1 = spheres[i].get();
			Object &sphere2 = spheres[j].get();

			float distance = glm::length(sphere1.position - sphere2.position);
			float radius = sphere1.scale.x + sphere2.scale.x;

			if (distance <= radius)
			{
				float m1 = sphere1.mass;
				float m2 = sphere2.mass;

				glm::vec3 x1 = sphere1.position;
				glm::vec3 x2 = sphere2.position;

				glm::vec3 connector = glm::normalize(x1 - x2);

				float r1 = sphere1.scale.x;
				float r2 = sphere2.scale.y;
				float scale = r1 + r2;
				sphere1.position = x2 + connector * scale;

				x1 =  sphere1.position;

				glm::vec3 v1 = sphere1.velocity;
				glm::vec3 v2 = sphere2.velocity;

				glm::vec3 dx = x1 - x2;
				float length = glm::length(dx);
				float lengthSquared = length * length;

				float k1 = (2 * m2) / (m1 + m2);
				float k2 = (2 * m1) / (m1 + m2);

				float d1 = glm::dot(v1 - v2, dx) / lengthSquared;
				float d2 = glm::dot(v2 - v1, -dx) / lengthSquared;

				glm::vec3 u1 = v1 - k1 * d1 * dx;
				glm::vec3 u2 = v2 - k2 * d2 * (-dx);

				sphere1.velocity = u1;
				sphere2.velocity = u2;
			}
		}
	}
}

void PhysicsEngine::collideSpherePlatform()
{
	for (auto &body: spheres)
	{
		Object &sphere = body.get();

		float radius = sphere.scale.x;

		float width = platform.scale.x;
		float height = platform.scale.y;
		float depth = platform.scale.z;

		glm::vec3 distance = glm::abs(sphere.position - platform.position);

		if (distance.x >= width + radius || distance.y >= height + radius || distance.z >= depth + radius)
		{
			continue;
		}

		if (distance.x < width || distance.y < height || distance.z < depth)
		{
			glm::vec3 normal{};
			normal = glm::sign(sphere.position - platform.position);

			if (distance.x < width && distance.y < height)
			{
				normal.x = 0;
				normal.y = 0;
				sphere.position.z = platform.position.z + depth + radius;
			} else if (distance.x < width && distance.z < depth)
			{
				normal.x = 0;
				normal.z = 0;
				sphere.position.y = platform.position.y + height + radius;
			} else if (distance.y < height && distance.z < depth)
			{
				normal.y = 0;
				normal.z = 0;
				sphere.position.x = platform.position.x + width + radius;
			} else if (distance.x < width)
			{
				normal.x = 0;
			} else if (distance.y < height)
			{
				normal.y = 0;
			} else if (distance.z < depth)
			{
				normal.z = 0;
			}

			normal = glm::normalize(normal);
			sphere.velocity = sphere.restitution * normal * platform.friction;
			continue;
		}

		float distanceToCorner = (distance.x - width) * (distance.x - width) +
								 (distance.y - height) * (distance.y - height) +
								 (distance.z - depth) * (distance.z - depth);

		if (distanceToCorner < radius * radius)
		{
			glm::vec3 normal = glm::normalize(glm::sign(sphere.position - platform.position));
			sphere.velocity = sphere.restitution * normal * platform.friction;
		}
	}
}
