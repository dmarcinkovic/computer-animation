//
// Created by david on 20. 03. 2020..
//

#include "Display.h"
#include "../bindings/imgui_impl_opengl3.h"
#include "../bindings/imgui_impl_glfw.h"

int Display::windowWidth{};
int Display::windowHeight{};

std::vector<ScrollListener> Display::listeners;

Display::Display(int width, int height, const char *title)
{
	windowWidth = width;
	windowHeight = height;

	glfwInit();

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(width, height, title, nullptr, nullptr);
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);

	setViewport(width, height);

	glfwSetWindowSizeCallback(window, windowResizeCallback);
	glfwSetScrollCallback(window, scrollCallback);
	glewInit();

	ImGui::CreateContext();
	ImGui::StyleColorsDark();
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init("#version 130");
}

Display::~Display()
{
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	glfwDestroyWindow(window);
	glfwTerminate();
}

void Display::update()
{
	double currentTime = glfwGetTime();
	deltaTime = currentTime - lastFrameTime;
	lastFrameTime = currentTime;

	glfwPollEvents();
	glfwSwapBuffers(window);
}

bool Display::isRunning() const
{
	return !glfwWindowShouldClose(window);
}

void Display::clearWindow()
{
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Display::windowResizeCallback(GLFWwindow *, int width, int height)
{
	setViewport(width, height);

	windowWidth = width;
	windowHeight = height;
}

int Display::getWidth()
{
	return windowWidth;
}

int Display::getHeight()
{
	return windowHeight;
}

void Display::setViewport(int width, int height)
{
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(0, width, 0, height, -1.5, 1.5);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

float Display::getDeltaTime() const
{
	return static_cast<float>(deltaTime);
}

void Display::scrollCallback(GLFWwindow *, double xOffset, double yOffset)
{
	for (auto const &listener : listeners)
	{
		listener(xOffset, yOffset);
	}
}

void Display::addScrollListener(const ScrollListener &listener)
{
	listeners.emplace_back(listener);
}
