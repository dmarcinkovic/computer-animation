//
// Created by david on 14. 10. 2021..
//

#include "ObjectRenderer.h"
#include "Maths.h"
#include "Display.h"
#include "Constants.h"

void ObjectRenderer::render(const Loader &loader, const Camera &camera, const Light &light) const
{
	prepareRendering(light, camera);

	for (auto const &object: objects)
	{
		Object &o = object.get();

		glm::mat4 viewMatrix = Maths::createViewMatrix(camera);
		objectShader.loadViewMatrix(viewMatrix);

		objectShader.loadObjectTexture();
		o.bindTexture();
		o.bindNormalMapTexture();

		objectShader.loadUseNormal(o.useNormalMap);
		objectShader.loadLight(light.position, light.color, o.shineDamper, o.material);

		renderObject(object.get(), loader);

		loader.unbindVao();
		Object::unbindTexture();
	}

	finishRendering();
}

void ObjectRenderer::addObject(Object &object)
{
	objects.emplace_back(object);
}

void ObjectRenderer::prepareRendering(const Light &light, const Camera &camera) const
{
	objectShader.start();

	auto width = static_cast<float>(Display::getWidth());
	auto height = static_cast<float>(Display::getHeight());

	float fov = Constants::FOV;
	float far = Constants::FAR;
	float near = Constants::NEAR;
	glm::mat4 projectionMatrix = Maths::createProjectionMatrix(fov, far, near, width, height);
	objectShader.loadProjectionMatrix(projectionMatrix);

	objectShader.loadCameraPosition(camera.getPosition());

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
}

void ObjectRenderer::finishRendering()
{
	ObjectShader::stop();
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
}

void ObjectRenderer::renderObject(const Object &object, const Loader &loader) const
{
	loader.bindVao(object.getModel());

	glm::mat4 modelMatrix = object.getModelMatrix();
	objectShader.loadTransformationMatrix(modelMatrix);

	auto count = static_cast<GLsizei>(object.getModel().vertexCount);
	glDrawArrays(GL_TRIANGLES, 0, count);
}

std::vector<std::reference_wrapper<Object>> &ObjectRenderer::getObjects()
{
	return objects;
}
