//
// Created by david on 14. 10. 2021..
//

#include "Object.h"
#include "Maths.h"

Object::Object(const Model &model, GLuint texture, GLuint normalTexture)
		: model(model), texture(texture), normalMapTexture(normalTexture)
{

}

glm::mat4 Object::getModelMatrix() const
{
	return Maths::createTransformationMatrix(position, rotation, scale);
}

const Model &Object::getModel() const
{
	return model;
}

void Object::bindTexture() const
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
}

void Object::bindNormalMapTexture() const
{
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, normalMapTexture);
}

void Object::unbindTexture()
{
	glBindTexture(GL_TEXTURE_2D, 0);
}
