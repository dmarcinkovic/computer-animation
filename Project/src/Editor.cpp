//
// Created by david on 03. 01. 2022..
//

#include <glm/gtc/type_ptr.hpp>
#include <string>

#include "Editor.h"
#include "../bindings/imgui_impl_glfw.h"
#include "../bindings/imgui_impl_opengl3.h"

bool Editor::isHovered{};
std::vector<Object> Editor::storage;

void Editor::render()
{
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

void Editor::newFrame()
{
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();
}

void Editor::draw(ObjectRenderer &renderer, Camera &camera, bool &play)
{
	storeObjects(renderer);

	if (ImGui::Begin("Settings"))
	{
		isHovered = ImGui::IsWindowHovered();

		if (ImGui::Button("Play"))
		{
			play = true;
		}

		ImGui::SameLine();

		if (ImGui::Button("Stop"))
		{
			play = false;
		}

		ImGui::SameLine();

		if (ImGui::Button("Reset"))
		{
			play = false;
			restoreObjects(renderer);
		}

		ImGui::Separator();

		drawCamera(camera);
		ImGui::Separator();

		auto &objects = renderer.getObjects();
		for (int i = 1; i < objects.size(); ++i)
		{
			Object &sphere = objects[i].get();

			ImGui::PushID(i);

			std::string name = "Sphere" + std::to_string(i);
			if (ImGui::CollapsingHeader(name.c_str()))
			{
				drawObjectProperties(sphere);
			}

			ImGui::PopID();
		}

		Object &platform = objects[0].get();
		if (ImGui::CollapsingHeader("Platform"))
		{
			drawObjectProperties(platform);
		}
	}

	ImGui::End();
}

void Editor::storeObjects(ObjectRenderer &renderer)
{
	if (!storage.empty())
	{
		return;
	}

	const auto &objects = renderer.getObjects();

	for (const auto &object : objects)
	{
		storage.emplace_back(object.get());
	}
}

void Editor::restoreObjects(ObjectRenderer &renderer)
{
	auto &objects = renderer.getObjects();

	for (int i = 0; i < objects.size(); ++i)
	{
		Object &object = objects[i].get();

		object.position = storage[i].position;
		object.rotation = storage[i].rotation;
		object.scale = storage[i].scale;
		object.velocity = glm::vec3{};
	}
}

void Editor::drawObjectProperties(Object &object)
{
	ImGui::DragFloat3("Position", glm::value_ptr(object.position));
	ImGui::DragFloat3("Rotation", glm::value_ptr(object.rotation));
	ImGui::DragFloat3("Scale", glm::value_ptr(object.scale));

	ImGui::DragFloat("Restitution", &object.restitution, 0.01f, 0.0f, 1.0f);
	ImGui::DragFloat("Friction", &object.friction, 0.01f, 0.0f, 1.0f);
	ImGui::DragFloat("Mass", &object.mass, 0.1f, 0.0f, 1000.0f);

	ImGui::DragFloat("Shine", &object.shineDamper, 0.01f, 0, 1);
	ImGui::DragInt("Material", &object.material, 1, 1, 5);

	ImGui::Checkbox("Use normal mapping", &object.useNormalMap);
}

bool Editor::isEditorHovered()
{
	return isHovered;
}

void Editor::drawCamera(Camera &camera)
{
	if (ImGui::CollapsingHeader("Camera"))
	{
		ImGui::DragFloat3("Camera position", glm::value_ptr(camera.getPosition()));
		ImGui::DragFloat("Camera pitch", &camera.getPitch());
		ImGui::DragFloat("Camera yaw", &camera.getYaw());
	}
}
