//
// Created by david on 21. 03. 2020..
//

#ifndef LAB3_OBJECTSHADER_H
#define LAB3_OBJECTSHADER_H

#include <GL/glew.h>
#include <glm/glm.hpp>

#include "Shader.h"

class ObjectShader : public Shader
{
private:
	constexpr static const char *VERTEX_FILE = "shader/MixVertexShader.glsl";
	constexpr static const char *FRAGMENT_FILE = "shader/MixFragmentShader.glsl";

	GLint locationTransformationMatrix{};
	GLint locationProjectionMatrix{};
	GLint locationViewMatrix{};

	GLint locationLightPosition{};
	GLint locationLightColor{};
	GLint locationShineDamper{};
	GLint locationMaterial{};

	GLint locationCameraPosition{};

	GLint locationObjectTexture{};
	GLint locationNormalMapTexture{};

	GLint locationUseNormal{};

public:
	ObjectShader();

	void loadTransformationMatrix(const glm::mat4 &transformationMatrix) const;

	void loadViewMatrix(const glm::mat4 &viewMatrix) const;

	void loadProjectionMatrix(const glm::mat4 &projectionMatrix) const;

	void loadLight(const glm::vec3 &lightPosition, const glm::vec3 &lightColor, float shineDamper, int material) const;

	void loadCameraPosition(const glm::vec3 &cameraPosition) const;

	void loadObjectTexture() const;

	void loadUseNormal(bool useNormal) const;

private:
	void getUniformLocations();
};


#endif //LAB3_OBJECTSHADER_H
