//
// Created by david on 24. 04. 2020..
//

#include <fstream>
#include <string>
#include <iostream>

#include "Shader.h"

Shader::Shader(const char *vertexFile, const char *fragmentFile)
{
	vertexShader = loadShader(vertexFile, GL_VERTEX_SHADER);
	fragmentShader = loadShader(fragmentFile, GL_FRAGMENT_SHADER);

	program = glCreateProgram();
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);

	glLinkProgram(program);
	glValidateProgram(program);
}

GLuint Shader::loadShader(const char *filename, GLenum type)
{
	std::ifstream reader(filename);

	if (!reader)
	{
		throw std::runtime_error("Cannot open shader file " + std::string(filename));
	}

	std::string source((std::istreambuf_iterator<char>(reader)),
					   std::istreambuf_iterator<char>());

	const char *shaderSource = source.c_str();

	GLuint shaderId = glCreateShader(type);
	glShaderSource(shaderId, 1, &shaderSource, nullptr);
	glCompileShader(shaderId);

	debug(shaderId);

	reader.close();
	return shaderId;
}

void Shader::debug(GLuint shaderId)
{
	int result;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &result);

	if (result == GL_FALSE)
	{
		int length;
		glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &length);
		char *message = (char *) alloca(length * sizeof(char));

		glGetShaderInfoLog(shaderId, length, &length, message);

		std::cout << "Error while compiling shader: " << message << "\n";
		glDeleteShader(shaderId);
	}
}

Shader::~Shader()
{
	glDetachShader(program, vertexShader);
	glDetachShader(program, fragmentShader);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	glDeleteProgram(program);
}

void Shader::start() const
{
	glUseProgram(program);
}

void Shader::stop()
{
	glUseProgram(0);
}

GLint Shader::getUniformLocation(const char *name) const
{
	return glGetUniformLocation(program, name);
}
