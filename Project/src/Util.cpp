//
// Created by david on 14. 10. 2021..
//

#include "Util.h"

std::vector<std::string> Util::split(const std::string &str, const std::string &delimiter)
{
	std::vector<std::string> tokens;
	size_t prev = 0, pos;
	do
	{
		pos = str.find(delimiter, prev);

		if (pos == std::string::npos) pos = str.length();
		std::string token = str.substr(prev, pos - prev);

		if (!token.empty()) tokens.push_back(token);
		prev = pos + delimiter.length();
	} while (pos < str.length() && prev < str.length());

	return tokens;
}
