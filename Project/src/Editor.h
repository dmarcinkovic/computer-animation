//
// Created by david on 29. 12. 2021..
//

#ifndef PROJEKT_EDITOR_H
#define PROJEKT_EDITOR_H

#include <vector>

#include "Object.h"
#include "ObjectRenderer.h"

class Editor
{
private:
	static std::vector<Object> storage;

	static bool isHovered;

public:
	static void newFrame();

	static void render();

	static void draw(ObjectRenderer &renderer, Camera &camera, bool &play);

	static void storeObjects(ObjectRenderer &renderer);

	static void restoreObjects(ObjectRenderer &renderer);

	static bool isEditorHovered();

private:
	static void drawObjectProperties(Object &object);

	static void drawCamera(Camera &camera);
};


#endif //PROJEKT_EDITOR_H
